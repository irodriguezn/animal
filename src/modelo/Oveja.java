package modelo;

import utilidades.Utilidades;
import static utilidades.Constantes.*;
import java.io.Serializable;

/**
 *
 * @author nachorod
 */
public class Oveja extends Animal implements Serializable {
    String idOveja;
    int kilosLana;
    static int numOvejas;
    
    public Oveja(String nombre) {
        super(nombre);
    }
    
    public void inicializar() {
        kilosLana=(int) (Math.random() * 15 + 21); // Entre 20 y 35 Kg
        velocidad=Utilidades.redondear(Math.random() * 10 + 26, 2); // Entre 25 y 35 Km/h
        direccionMirada=devuelveOrientacion();
        if (Math.random()<0.5) {
            color=NEGRO;
        } else {
            color=BLANCO;
        }
        numOvejas++;
        idOveja="Oveja" + numOvejas;
        edadGenetica=70000; // edad límite aproximada en pasos
        edadMinReproduccion=2000;
        edadMaxReproduccion=55000;
        numeroMedioCrias=3;
        numeroMaxCrias=6;
        periodoGestacion=300;                
    }
    
    public String getIdOveja() {
        return idOveja;
    }
    
    @Override
    public String toString() {
        String cadena="";
        cadena+=super.toString();
        cadena+="Kilos de lana: " + kilosLana + "\n";
        return cadena;
    }

    public int getKilosLana() {
        return kilosLana;
    }

    public void setKilosLana(int kilosLana) {
        this.kilosLana = kilosLana;
    }
    
    public static void getNumOvejas() {
        System.out.println("Número de ovejas: " + numOvejas);
    }
    
    public String comunicar() {
         return "Beeeeeeeeeeeeeee!";
    }
    
    protected void finalize() {
        numOvejas--;
    }
    
}

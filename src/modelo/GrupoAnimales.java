/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Arrays;
import java.util.LinkedList;
import java.io.Serializable;


/**
 *
 * @author nacho
 */
// Clase que representa un grupo homogéneo o heterogéneo de animales. 
// El nombre es opcional y representa las agrupaciones homogéneas de
// animales con nombre, por ejemplo un rebaño de ovejas.
public class GrupoAnimales implements Serializable {
    private String nombre;
    private int idGrupo;
    static int numGrupos;
    private final LinkedList<Animal> animales;
    
    public GrupoAnimales() {
        animales=new LinkedList<Animal>();
        numGrupos++;
        idGrupo=numGrupos;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre=nombre;
    }
    
    public void añadir(Animal a, int pos) {
        animales.add(pos, a);
    }
    
    public boolean añadir(Animal a){
        if (animales.add(a)) {
            return true;
        } else {
            return false;
        }
    }
    
    public int size(){
        return animales.size();
    }
    
    public Animal extraer(int pos) {
        return animales.get(pos);
    }
    
    public Animal extraerUltimo(){
        return animales.getLast();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.io.Serializable;

/**
 *
 * @author alumno
 * 
 */
public class Mapa implements Serializable {
    private final int numCasillas=20;
    private Object[][] mapa=new Object[numCasillas+2][numCasillas+2];
    private GrupoAnimales lobos=new GrupoAnimales();
    private GrupoAnimales ovejas=new GrupoAnimales();
    private long pasosTranscurridos; //pasos transcurridos desde el inicio
    final private int paso=1; //horas por paso
    
    
    public void rellenaOvejas(int num) {
        int cuentaOvejas=0;
        int fila, columna;
        int [] distProbaOveja={20, 37, 51, 62, 72, 82, 91, 97, 100, 0, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0}; 
        while (cuentaOvejas!=num) {
            fila=aleatorioDistribucion(distProbaOveja);
            columna=aleatorioDistribucion(distProbaOveja);
            if (mapa[fila][columna]==null) {
                cuentaOvejas++;
                Oveja ov=new Oveja("O"+cuentaOvejas);
                ov.inicializar();
                ov.setFila(fila);
                ov.setColumna(columna);
                mapa[fila][columna]=ov;
                ov.setMapa(this);
                ovejas.añadir(ov);
            }
        }
    }

    public void rellenaLobos(int num) {
        int [] distProbaLobo={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 9, 18, 28, 38, 49, 63, 80, 100}; 
        int cuentaLobos=0;
        int fila, columna;
        while (cuentaLobos!=num) {
            fila=aleatorioDistribucion(distProbaLobo);
            columna=aleatorioDistribucion(distProbaLobo);
            /*fila=(int)(Math.random()*numCasillas+1);
            columna=(int)(Math.random()*numCasillas+1);*/
            if (mapa[fila][columna]==null) {
                cuentaLobos++;
                Lobo lo=new Lobo("L"+cuentaLobos);
                lo.inicializar();
                lo.setColumna(columna);
                lo.setFila(fila);
                mapa[fila][columna]=lo;
                lo.setMapa(this);
                lobos.añadir(lo);
            }
        }
    }
    
    public void rellenaParedes(int num) {
        int cuentaParedes=0;
        int fila, columna;
        while (cuentaParedes!=num) {
            fila=(int)(Math.random()*numCasillas+1);
            columna=(int)(Math.random()*numCasillas+1);
            if (mapa[fila][columna]==null) {
                cuentaParedes++;
                mapa[fila][columna]="P";
            }
        }
        // Ahora rellenamos los bordes del mapa con paredes
        for (int i=0; i<numCasillas+2; i++) {
            mapa[i][0]="P";
            mapa[i][numCasillas+1]="P";
        }
        for (int j=0; j<numCasillas+2; j++) {
            mapa[0][j]="P";
            mapa[numCasillas+1][j]="P";
        }
    }
    
    public Object[][] getMapa() {
        return mapa;
    }
    
    public GrupoAnimales getOvejas(){
        return ovejas;
    }
    
    public GrupoAnimales getLobos(){
        return lobos;
    }
    
    public int getPaso(){
        return paso;
    }
    
    public long getPasosTranscurridos() {
        return pasosTranscurridos;
    }
    
    public void incrementaPasos(int incremento) {
        pasosTranscurridos+=incremento;
    }
    
    static public int aleatorioDistribucion(int [] distribucion) {
        int aleatorio;
        int resultado=0;
        aleatorio=(int)(Math.random()*100+1);
        for (int i=1; i<=distribucion.length; i++) {
            if (aleatorio<=distribucion[i-1]) {
                    resultado=i;
                    break;
            } 
        }
        return resultado;
    }
}

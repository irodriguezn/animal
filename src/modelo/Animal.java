/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.ArrayList;
import utilidades.Constantes.Orientacion;
import java.io.Serializable;

/**
 *
 * @author nachorod
 */
public abstract class Animal implements Serializable {
    protected String nombre;
    protected boolean sexo; //Verdadero: Hembra - Falso: Macho
    protected String color;
    protected double velocidad;
    protected int salud;
    protected Orientacion direccionMirada;
    protected int fila;
    protected int columna;
    protected long edad; // En pasos o ciclos de simulación
    protected long edadGenetica; // edad límite en pasos
    protected long edadMinReproduccion;
    protected long edadMaxReproduccion;
    protected int periodoGestacion;
    protected int numeroMedioCrias;
    protected int numeroMaxCrias;
    
    protected Mapa mapa;
    
    
    
    public Animal(String nombre) {
        this.nombre=nombre;
    }

    public long getEdad() {
        return edad;
    }

    public void setEdad(long edad) {
        this.edad = edad;
    }

    public long getEdadGenetica() {
        return edadGenetica;
    }

    public void setEdadGenetica(long edadGenetica) {
        this.edadGenetica = edadGenetica;
    }

    public long getEdadMinReproduccion() {
        return edadMinReproduccion;
    }

    public void setEdadMinReproduccion(long edadMinReproduccion) {
        this.edadMinReproduccion = edadMinReproduccion;
    }

    public long getEdadMaxReproduccion() {
        return edadMaxReproduccion;
    }

    public void setEdadMaxReproduccion(long edadMaxReproduccion) {
        this.edadMaxReproduccion = edadMaxReproduccion;
    }

    public int getPeriodoGestacion() {
        return periodoGestacion;
    }

    public void setPeriodoMedioGestacion(int periodoMedioGestacion) {
        this.periodoGestacion = periodoMedioGestacion;
    }

    public int getNumeroMedioCrias() {
        return numeroMedioCrias;
    }

    public void setNumeroMedioCrias(int numeroMedioCrias) {
        this.numeroMedioCrias = numeroMedioCrias;
    }

    public int getNumeroMaxCrias() {
        return numeroMaxCrias;
    }

    public void setNumeroMaxCrias(int numeroMaxCrias) {
        this.numeroMaxCrias = numeroMaxCrias;
    }

    public Mapa getMapa() {
        return mapa;
    }

    public void setMapa(Mapa mapa) {
        this.mapa = mapa;
    }
    

    public boolean esHembra() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setEdadGenetica(int edadGenetica) {
        this.edadGenetica = edadGenetica;
    }
        
    public int getSalud() {
        return salud;
    }

    public void setSalud(int salud) {
        this.salud = salud;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public Orientacion getDireccionMirada() {
        return direccionMirada;
    }

    public void setDireccionMirada(Orientacion DireccionMirada) {
        this.direccionMirada = DireccionMirada;
    }
    
    public void actualizarEstado() {
        edad++;
        boolean puedeReproducirse=(edad<=edadMaxReproduccion && edad>=edadMinReproduccion);
        doMovimiento(this);
    }
    
    @Override
    public String toString() {
        String cadena="";
        cadena+="Nombre: " + nombre + "\n";
        cadena+="Color: " + color + "\n";
        cadena+="Salud: " + salud + "\n";
        cadena+="Velocidad: " + velocidad + "\n";
        return cadena;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

        public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }
    
    public abstract String comunicar();

    static void doMovimiento(Animal a) {
        ArrayList<Orientacion> movimientosPosibles=new ArrayList<>();
        movimientosPosibles.add(Orientacion.norte);
        movimientosPosibles.add(Orientacion.este);
        movimientosPosibles.add(Orientacion.sur);
        movimientosPosibles.add(Orientacion.oeste);
        int fila, columna;
        Mapa mapa=a.mapa;
        boolean salir=false;
        while (!salir) {
            fila=a.fila;
            columna=a.columna;
            int direccion=(int)(Math.random()*movimientosPosibles.size());
            Orientacion orientacionElegida=movimientosPosibles.get(direccion);
            movimientosPosibles.remove(direccion);
            switch (orientacionElegida) {
                case norte:
                    fila--;
                    break;
                case este:
                    columna++;
                    break;
                case sur:
                    fila++;
                    break;
                case oeste:
                    columna--;
                    break;
            }
        
            Object obj=mapa.getMapa()[fila][columna];
            if (obj==null) {
                int filaAnt=a.fila;
                int colAnt=a.columna;
                a.fila=fila;
                a.columna=columna;
                mapa.getMapa()[fila][columna]=a;
                mapa.getMapa()[filaAnt][colAnt]=null;
                salir=true;
            } else {
                if (movimientosPosibles.isEmpty()) {
                    salir=true;
                }
            }
        }
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import utilidades.Utilidades;
import static utilidades.Constantes.*;
import java.io.Serializable;

/**
 *
 * @author nachorod
 */

/**
 * Longevidad:  Puede vivir hasta los 14 ó 15 años, aun cuando lo normal es que no supere los 6-8 años en estado salvaje.
 
Celo:  Un solo celo, entre finales de enero y principios de abril.
 
Gestación: La gestación dura unos 60 días.
 
Época de parto:  El parto suele coincidir el final de la primavera.
 
Parto: El lobo tiene una camada anualmente, dando a luz de entre tres a ocho cachorros, entre mayo y junio. 
 * 
 * 
*/
public class Lobo extends Animal implements Serializable {
    public Lobo(String nombre) {
        super(nombre);
    }
    
    public void inicializar(){
        velocidad=Utilidades.redondear(Math.random() * 10 + 36, 2); // Entre 35 y 45 Km/h
        direccionMirada=devuelveOrientacion();
        color=ROJO;
        edadGenetica=50000; // edad límite aproximada en pasos
        edadMinReproduccion=1000;
        edadMaxReproduccion=35000;
        numeroMedioCrias=6;
        numeroMaxCrias=9;
        periodoGestacion=200;        
    }
    
    public boolean atacar(Oveja ov1) {
        int fuerzaAtaque=(int)(Math.random()*100+76);
        int saludOveja=ov1.getSalud();
        saludOveja-=fuerzaAtaque;
        if (saludOveja<=0) {
            ov1.setSalud(0);
            return true;
        } else {
            ov1.setSalud(saludOveja);
            return false;
        }
    }
    
    public String comunicar() {
        return "Aaaauuuuuuuuuuuuuuuuuu";
    }
}

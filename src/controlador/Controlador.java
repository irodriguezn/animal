/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import vista.*;

/**
 *
 * @author nachorod
 */
public class Controlador {

    private GrupoAnimales grupoAnimales;
    private Vista vista;
    
    public Controlador(GrupoAnimales grupoAnimales, Vista vista) {
        this.grupoAnimales=grupoAnimales;
        this.vista=vista;
        menuPrincipal();
    }
    
    private void menuPrincipal() {
  
        Mapa mapa=null;
        VistaMapa vistaMapa=new VistaMapa();
        int opcion;
        do {
            opcion=vista.menuPrincipal();
            switch (opcion) {
                case 1:
                    mapa=inicializar();
                    simular(mapa);
                    break;
                case 2:
                    if (mapa!=null) {
                        simular(mapa);
                    } else {
                        vista.mostrarMensaje("\nNo hay simulación activa");
                    }
                    break;
                case 3: // Cargar simulación guardada en disco
                    mapa=vistaMapa.cargarMapa();
                    if (mapa!=null) {
                        vista.mostrarMensaje("\nSimulación cargada");
                    }
                    break;
                case 4: // Guardar simulación en disco
                    if (mapa!=null) {
                        if (vistaMapa.guardarMapa(mapa)) {
                            vista.mostrarMensaje("\nSimulación guardada");
                        }
                    } else {
                        vista.mostrarMensaje("\nNo hay simulación activa");
                    }
                    break;
                case 5:
                    vista.mostrarMensaje("\nAdios!");
                    break;
            }
            System.out.println("");
        } while (opcion!=0);
    }
    
    private Mapa inicializar(){
        Mapa mapa=new Mapa();
        mapa.rellenaLobos(5);
        mapa.rellenaOvejas(9);
        mapa.rellenaParedes(15);
        return mapa;
    }
    
    private void simular(Mapa mapa) {
        Vista vista=new Vista();
        VistaMapa vistaMapa=new VistaMapa();
        vistaMapa.mostrarMapa(mapa);
        int pasosAnterior=0;
        int op=vista.menu();
        int pasos=vistaMapa.pasosAvanzar(op, pasosAnterior);

        while (pasos!=0) {
            for (int i=1; i<=pasos; i++) {
                GrupoAnimales ovejas=mapa.getOvejas();
                Oveja ov;
                for (int j=0; j<ovejas.size(); j++) {
                    ov=(Oveja)ovejas.extraer(j);
                    ov.actualizarEstado();
                }
                GrupoAnimales lobos=mapa.getLobos();
                Lobo lo;
                for (int j=0; j<lobos.size(); j++) {
                    lo=(Lobo)lobos.extraer(j);
                    lo.actualizarEstado();
                }
            }
            mapa.incrementaPasos(pasos);
            vistaMapa.mostrarMapa(mapa);
            pasosAnterior=pasos;
            pasos=vistaMapa.pasosAvanzar(vista.menu(), pasosAnterior);
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.File;
import modelo.*;
import static utilidades.Constantes.*;
import utilidades.Utilidades;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;

/**
 *
 * @author alumno
 */
public class VistaMapa {

    public void mostrarMapa(Mapa m) {
        Oveja ov;
        Lobo lo;
        VistaOveja vo=new VistaOveja();
        VistaLobo vl= new VistaLobo();
        Object[][] mapa=m.getMapa();
        System.out.println("");
        System.out.println("Tiempo Transcurrido: "+ Utilidades.añosDiasHoras(m.getPaso()*m.getPasosTranscurridos()));
        for (int i=1; i<mapa.length-1; i++) {
            for (int j=1; j<mapa[0].length-1; j++) {
                if (mapa[i][j]==null) {
                    System.out.print(NEGRO+(char)9601+NEGRO+" ");
                } else if (mapa[i][j] instanceof Oveja) {
                    ov=(Oveja)mapa[i][j];
                    System.out.print(vo.mostrarOveja(ov));
                } else if (mapa[i][j] instanceof Lobo) {
                    lo=(Lobo)mapa[i][j];
                    System.out.print(vl.mostrarLobo(lo));
                } else if (mapa[i][j].equals("P")) {
                    System.out.print(CIAN+(char)9608+NEGRO+" ");
                }
            }
            System.out.println("");

        }
        System.out.println("");
    }
    
    public int pasosAvanzar(int op, int pasos) {
        switch (op) {
            case 0:
                pasos=0;
                break;
            case 1: 
                pasos=1;
                break;
            case 2:
                pasos=10;
                break;
            case 3:
                pasos=Vista.validarEntero("Introduzca número de pasos (1 a 10.000): ");
        }
        return pasos;
    }    
    
    public boolean guardarMapa(Mapa mapa) {
        boolean guardada=false;
        String nombre=Vista.validarString("\nIntroduzca el nombre del archivo: ", 20);
        File f=new File(nombre);
        System.out.println("");
        if (f.exists()) {
            String op=Vista.validarString("El nombre del fichero ya existe, ¿desea sobreescribirlo(S/N)?", 1);
            if (op.equalsIgnoreCase("s")) {
                        try {
                            ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(nombre));
                            os.writeObject(mapa);
                            guardada=true;
                            os.close();
                        } catch (Exception e) {
                            System.out.println("Error escribiendo el archivo");
                        }
            }
        } else {
                try {
                    ObjectOutputStream os=new ObjectOutputStream(new FileOutputStream(nombre));
                    os.writeObject(mapa);
                    guardada=true;
                    os.close();
                } catch (Exception e) {
                    System.out.println("Error escribiendo el archivo");
                }            
        }
        return guardada;
    }
    
    public Mapa cargarMapa() {
        Mapa mapa=null;
        String nombre=Vista.validarString("\nIntroduzca el nombre del archivo: ", 20);
        File f=new File(nombre);
        if (f.exists()) {
                try {
                    ObjectInputStream is=new ObjectInputStream(new FileInputStream(nombre));
                    mapa=(Mapa)is.readObject();
                    is.close();
                } catch (Exception e) {
                    System.out.println("Error leyendo el archivo");
                }
        } else {
            System.out.println("\nFichero inexistente!");
        }        
        return mapa;
    }
}

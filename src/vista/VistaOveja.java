/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Oveja;
import static utilidades.Constantes.*;
//import modelo.Constantes.Orientacion;
/**
 *
 * @author nacho
 */
public class VistaOveja {
    
    public String mostrarOveja(Oveja ov) {
        String color=ov.getColor();
        String resultado=NEGRO;
        switch (color) {
            case BLANCO:
                resultado=NEGRO+OVEJA_BLANCA+NEGRO;
                break;
            case NEGRO:
                resultado=NEGRO+OVEJA_NEGRA+NEGRO;
                break;
        }
        return resultado+" ";
    }
}

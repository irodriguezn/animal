/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Lobo;
import static utilidades.Constantes.*;

/**
 *
 * @author nacho
 */
public class VistaLobo {
    public String mostrarLobo(Lobo lo) {
        String resultado=lo.getColor();
        switch (lo.getDireccionMirada()) {
            case este:
                resultado+=LOBO_NORTE+NEGRO;
                break;
            case oeste:
                resultado+=LOBO_NORTE+NEGRO;
                break;
            case norte:
                resultado+=LOBO_NORTE+NEGRO;
                break;
            case sur:
                resultado+=LOBO_NORTE+NEGRO;
                break;
        }
        return resultado+" ";
    }    
}

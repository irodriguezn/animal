/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;
import utilidades.Utilidades;

/**
 *
 * @author alumno
 */
public class Vista {
    public String pausa(){
        System.out.print("Pulse intro para continuar (0 - Salir): ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
    
    public int menuPrincipal() {
        int op;
        System.out.println("MENÚ PRINCIPAL");
        System.out.println("--------------");
        System.out.println("1- Comenzar Simulación");
        System.out.println("2- Proseguir Actual");
        System.out.println("3- Cargar Simulación");
        System.out.println("4- Guardar Simulación");
        System.out.println("0- Salir");
        op=validarEntero("Seleccione opción (0-4): ", 0, 4);
        return op;
    }
    
    public int menu() {
        return validarEntero("0.FINALIZAR | 1.AVANZA 1 | 2.AVANZA 10 | 3.AVANZA X | 4.REPETIR: ",0,4);
    }
    
    public void mostrarMensaje(String msg) {
        System.out.println(msg);
    }
    
    public static int validarEntero(String mensaje) {
        boolean salir=false;
        int enteroValidado=0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                enteroValidado=sc.nextInt();
                salir=true;
            } catch (Exception e) {
                System.out.println("Opción no válida");
            } finally {
                sc.nextLine(); // Vacío buffer scanner
            }
        } while (!salir);
        return enteroValidado;
    }
    
    public static int validarEntero(String mensaje, int min, int max) {
        boolean salir=false;
        int enteroValidado=0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                enteroValidado=sc.nextInt();
                if (enteroValidado>=min && enteroValidado<=max) {
                    salir=true;
                } else {
                    System.out.println("El número ha de estar comprendido entre " + min + " y " + max);
                }
            } catch (Exception e) {
                System.out.println("Opción no válida");
            } finally {
                sc.nextLine(); // Vacío buffer scanner
            }
        } while (!salir);
        return enteroValidado;
    }
        
    public static String validarString(String mensaje, int numCaracteresMax) {
        boolean salir=false;
        String cadenaValidada="";
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                cadenaValidada=sc.next();
                if (cadenaValidada.length()<=numCaracteresMax) {
                    salir=true;
                } else {
                    System.out.println("El número de caracteres máximo es " + numCaracteresMax);
                }
            } catch (Exception e) {
                System.out.println("Opción no válida");
            }
        } while (!salir);
        return cadenaValidada;
    }        
    
    public static void pausar() {
        Scanner sc = new Scanner(System.in);
        System.out.print("<Pulse intro para continuar> ");
        sc.nextLine();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

/**
 *
 * @author nacho
 */
public class Constantes {
    
    
    static public Orientacion devuelveOrientacion(){
        Orientacion o=Orientacion.norte;
        switch ((int)(Math.random()*4)) {
            case 0:
                o=Orientacion.norte;
                break;
            case 1:
                o=Orientacion.sur;
                break;
            case 2:
                o=Orientacion.este;
                break;
            case 3:
                o=Orientacion.oeste;
                break;                
        }
        return o;
    }
    
    public enum Orientacion {
        norte, sur, este, oeste;
    }
    
    public static final char OVEJA_NORTE=8896;
    public static final char OVEJA_SUR=8897;
    public static final char OVEJA_ESTE='>';
    public static final char OVEJA_OESTE='<';
    public static final char OVEJA_BLANCA=9675;
    public static final char OVEJA_NEGRA=9679;
    public static final char LOBO_NORTE=9650; //Triángulo sólido arriba
    public static final char LOBO_SUR=9650;
    public static final char LOBO_ESTE=9650;
    public static final char LOBO_OESTE=9650;  
    public static final char PARED=9612;  
    
    public static final String NEGRO="\033[30m";
    public static final String ROJO="\033[31m";
    public static final String VERDE="\033[32m";
    public static final String AMARILLO ="\033[33m";
    public static final String AZUL= "\033[34m";
    public static final String PURPURA= "\033[35m";
    public static final String CIAN= "\033[36m";
    public static final String BLANCO = "\033[37m";
    
}

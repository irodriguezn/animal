/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Utilidades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static double redondear(double num, int decimales) {
        num=num*Math.pow(10, decimales);
        num=Math.ceil(num);
        num=num/Math.pow(10, decimales);
        return num;
    }    
    

    
    public static String añosDiasHoras(long horas) {
        String resultado="";
        int dias=(int)horas/60;
        horas=horas%60;
        int años=(int)dias/365;
        dias=dias%365;
        if (dias==0 && años==0) {
            resultado=horas+" horas";
        } else if (años==0) {
            resultado=dias + "d " + horas + "h ";
        } else {
            resultado=años + "años " + dias + "d " + horas + "h ";
        }
        return resultado;
    }
    
}
